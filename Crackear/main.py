import kivy
kivy.require('1.9.1')

from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.video import Video
from subtitle import *

FILENAME = '/home/vlad/test.ttml'

class VideoBox(Video):
	def __init__(self,**kwargs):
		super(VideoBox,self).__init__(**kwargs)
		self.subtext = Subtitle(FILENAME)		
		self.repeat_pos = 0
		self.textLabel = Label()

	def set_new_points(self,pause):
		self.subtext.set_new_points(pause)

	def set_play_pos(self):
		self.play_pos = self.position/self.duration

	def show_subtitle(self):
		self.textLabel.text = self.subtext.text
		#self.textLabel.center = self.center
		self.add_widget(self.textLabel)

	def pause_(self):
		self.state = 'pause'
		self.set_new_points(self.position)
		self.set_play_pos()
		print('position:',self.position)
		print('points:',self.subtext.points)
		print('play_pos',self.play_pos)
		self.show_subtitle()

	def play_(self):
		self.seek(self.repeat_pos)
		self.remove_widget(self.textLabel)
		self.state = 'play'
				
class Crackear(BoxLayout):
	pass
		
class CrackearApp(App):
	
	def build(self):
		crackear = Crackear()
		return crackear

if __name__ == "__main__":
	CrackearApp().run()
