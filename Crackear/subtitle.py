from bs4 import BeautifulSoup as bs

def get_captions(filename):

	with open(filename) as f:
		soup = bs(f.read(),'lxml')

	return soup.select('div p')

def element_to_structure(element):
	result = {}
	time = get_time_from_text(element.get('begin'))
	result['begin'] = time
	time = get_time_from_text(element.get('end'))
	result['end'] = time
	text = element.get_text(' ')
	text = text.replace('  ',' ')
	result['text'] = text

	return result

def get_time_from_text(text):
	splited_text = text.split(':')
	hours = float(splited_text[0])*3600.0
	minutes = float(splited_text[1])*60.0
	seconds = float(splited_text[-1])

	return hours+minutes+seconds
		
class Subtitle:

	def __init__(self,filename = None):

		self.filename = filename or ''
		self.points = [0,0]
		self.text = ''

		self.captions = get_captions(filename)
		self.set_minimum_delta()
		self.make_subtitle()

	def set_new_points(self,pause):
		old_1st_point = self.points[0]
		points = self.points[:]

		if pause > points[1]:
			points[0] = points[1]
		points[1] = pause

		self.points = points[:]
		self.change_text()
		if self.text == '':
			self.points[0] = old_1st_point
			self.change_text()

	def make_subtitle(self):
		result = []
		for element in self.captions:
			result += [element_to_structure(element)]
		self.subtitle = result
	
	def change_text(self):
		result = []
		self.text = ''

		start, finish = self.points
		for phrase in self.subtitle:
			if phrase['begin'] >= start and phrase['end'] <= finish:
				result += [' '.join(phrase['text'].split(' '))]
		if result:
			self.text = ' '.join(result)

	def set_minimum_delta(self):
		times = []
		for caption in self.captions:
			end = get_time_from_text(caption['end'])
			begin = get_time_from_text(caption['begin'])
			delta = end - begin 
			times += [delta]

		self.minimum_delta = min(times)		

	def make_new_text(self,pause):
		self.change_points(pause)
		self.change_text()

if __name__ == '__main__':
	pass	
