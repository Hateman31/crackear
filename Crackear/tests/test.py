import unittest
import os,sys
sys.path.append(os.path.abspath('../../'))
#
from Crackear.subtitle import Subtitle
from Crackear import subtitle
#from subtitle import Subtitle


class TestSubtitleMethods(unittest.TestCase):

	def setUp(self):
		self.filename = '/home/vlad/test.ttml'
		self.subtext = Subtitle(self.filename)

	def test_makeSubtitle(self):
		caption = self.subtext.subtitle[1]
		text = 'Could we move beyond our small blue planet'
		self.assertEqual(caption['text'],text)

	def test_textFromElement(self):
		text = 'Could we move beyond our small blue planet'
		caption = self.subtext.captions[1]
		structure = subtitle.element_to_structure(caption)
		self.assertEqual(text,structure['text'])

	def test_captionsExist(self):
		self.assertTrue(self.subtext.captions)

	def test_captionsElement(self):
		self.assertTrue(self.subtext.captions)

	def test_subtextNotEmpty(self):
		self.subtext.set_new_points(13.0)
		self.assertFalse(self.subtext.text == '')
		self.assertFalse(self.subtext.text is None)

	def test_subtext(self):
		self.subtext.set_new_points(13.0)
		self.assertEqual(self.subtext.text,'Could human civilization eventually spread across the whole Milky Way galaxy?')

	def test_setPointsCorrect(self):
		self.subtext.set_new_points(13.0)
		self.assertTrue(self.subtext.points[1] == 13.0)
		self.assertTrue(self.subtext.points[0] == 0.0)
		self.subtext.set_new_points(13.3)
		self.assertEqual(self.subtext.points,[0.0,13.3])
		
	def test_setAnotherPointsCorrect(self):
		self.subtext.set_new_points(13.0)
		self.subtext.set_new_points(31.0)
		self.assertTrue(self.subtext.points == [13.0,31.0])

	def test_setAnotherPointsCorrectReverse(self):
		self.subtext.set_new_points(31.0)
		self.subtext.set_new_points(13.0)
		self.assertEqual(self.subtext.points,[0,13.0])

	def test_makeTextCorrect(self):
		self.subtext.set_new_points(13.0)
		text = self.subtext.text
		self.subtext.set_new_points(13.3)
		self.assertEqual(self.subtext.text,text)

	def test_makeTextCorrect3(self):
		self.subtext.set_new_points(20.0)
		text = self.subtext.text
		self.assertEqual(self.subtext.points,[0,20.0])
		self.assertNotEqual('Could human civilization eventually spread across the whole Milky Way galaxy? Could we move beyond our small blue planet',text)
		self.subtext.set_new_points(22.0)
		text = self.subtext.text
		self.assertEqual(self.subtext.points,[0,22.0])
		self.assertNotEqual('Could human civilization eventually spread across the whole Milky Way galaxy? Could we move beyond our small blue planet',text)
		
	def test_makeTextCorrect2(self):
		self.subtext.set_new_points(20.0)
		text = self.subtext.text
		self.assertEqual(self.subtext.points,[0,20.0])
		self.assertNotEqual('Could human civilization eventually spread across the whole Milky Way galaxy? Could we move beyond our small blue planet',text)
		self.subtext.set_new_points(26.0)
		text = self.subtext.text
		self.assertEqual(self.subtext.points,[20.0,26.0])
		self.assertNotEqual('Could human civilization eventually spread across the whole Milky Way galaxy? Could we move beyond our small blue planet',text)
		self.assertEqual('There are around 300 billion stars in the galaxy,',text)

	def test_changeTextCorrect(self):
		self.subtext.set_new_points(12.0)
		self.subtext.set_new_points(16.0)
		text = self.subtext.text
		self.assertEqual('Could we move beyond our small blue planet',text)
		self.subtext.set_new_points(22.0)
		self.assertEqual(self.subtext.points,[16.0,22.0])
		text = "This question's a pretty daunting one."
		self.assertEqual(text,self.subtext.text)

	def test_setPointZero(self):
		self.subtext.set_new_points(0)

	def test_pointsWithoutSubtitle(self):
		self.subtext.set_new_points(2.0)
		self.assertEqual(self.subtext.text,'')
		self.assertEqual(self.subtext.points,[0,2.0])
		self.subtext.set_new_points(14.0)
		self.assertEqual(self.subtext.points,[2.0,14.0])
		self.assertEqual(self.subtext.text,'Could human civilization eventually spread across the whole Milky Way galaxy?')
		self.subtext.set_new_points(19.4)
		self.assertEqual(self.subtext.text,'Could human civilization eventually spread across the whole Milky Way galaxy? Could we move beyond our small blue planet')
		self.assertEqual(self.subtext.points,[2.0,19.4])

	def test_pointsWithSubtitlte(self):
		self.subtext.set_new_points(13.0)
		self.subtext.set_new_points(16.0)
		text = self.subtext.text
		self.assertEqual('Could human civilization eventually spread across the whole Milky Way galaxy? Could we move beyond our small blue planet',text)
		self.subtext.set_new_points(19.0)
		self.assertEqual('Could human civilization eventually spread across the whole Milky Way galaxy? Could we move beyond our small blue planet',text)
		self.assertEqual(self.subtext.points,[0,19.0])
		self.subtext.set_new_points(20.0)
		text = self.subtext.text
		self.assertEqual(self.subtext.points,[0,20.0])
		self.assertNotEqual('Could human civilization eventually spread across the whole Milky Way galaxy? Could we move beyond our small blue planet',text)
		
if __name__ == '__main__':
	unittest.main()
